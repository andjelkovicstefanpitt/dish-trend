# DiSH-trend

This is a Bitbucket repository of DiSH-trend simulation tool, accompanying a paper submitted to the Winter Simulation Conference 2021. The simulations are based on discrete network models for decision making in complex systems.

## Supplementary files

The directory Simulation contains the simulation tool. There are additional bash scripts: toy_models_run.sh which runs simulations of small 4-element toy models, and ETH_run.sh which runs simulations of large models. XLSX files are model spreadsheets that encode all necessary dynamic network model information for the simulator to run. There are PNG files showing the underlying graphs of smaller and larger models. WSC2021_data contains raw unprocessed data used for initializing models. Other directories contain simulation and visualization outputs.