SCENARIOS='WSC2021_ETH_model_level WSC2021_ETH_model_trend WSC2021_ETH_model_hybrid'
FRAMEWORKPATH='DiSH-trend'
STEPS=170
# MODELNAME='shock_demo'
WORKPATH='./'
CURRENTOUT='Ethiopia/'

mkdir "${WORKPATH}"
mkdir "${WORKPATH}${CURRENTOUT}"
for S in $SCENARIOS
do
	# SCENARIONAME=${S}
	mkdir "${WORKPATH}${CURRENTOUT}${S}"
	INPUTFILE="${WORKPATH}Ethiopia_models/${S}.xlsx"	
	OUTPUTFILE="${WORKPATH}${CURRENTOUT}${S}/${S}_traces.txt"
	OUTPUTMODEL="${WORKPATH}${CURRENTOUT}${S}/${S}.xlsx"

	cp ${INPUTFILE} ${OUTPUTMODEL}

	python3 ${FRAMEWORKPATH}/Simulator_Python/simulator_interface.py \
	${INPUTFILE} \
	${OUTPUTFILE} \
	--output_format=3 \
	--runs=200 \
	--steps=${STEPS} \
	--sim_scheme=ra \
	--timed
done