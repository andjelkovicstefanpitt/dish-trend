FRAMEWORKPATH='.'

STEPS=100

echo -e "\n\nTesting simulation, frequency summary output\n"
# default trace file output (frequency summary only)
python3 ${FRAMEWORKPATH}/Simulation/Simulator_Python/simulator_interface.py \
example_models/example_model.xlsx \
example_traces/example_traces.txt \
--steps=${STEPS} 

echo -e "\n\nTesting simulation, multiple scenarios\n"
# Running multiple scenarios
python3 ${FRAMEWORKPATH}/Simulation/Simulator_Python/simulator_interface.py \
example_models/example_model.xlsx \
example_traces/example_traces_multi.txt \
--scenarios=0,1,2 \
--steps=${STEPS}

echo -e "\n\nTesting simulation, all runs output\n"
# all simulation runs trace file output
python3 ${FRAMEWORKPATH}/Simulation/Simulator_Python/simulator_interface.py \
example_models/example_model.xlsx \
example_traces/example_traces_all.txt \
--output_format=1 \
--runs=100 \
--steps=${STEPS} \
--randomize_each_run

echo -e "\n\nTesting simulation, event trace output\n"
# event trace output
python3 ${FRAMEWORKPATH}/Simulation/Simulator_Python/simulator_interface.py \
example_models/example_model.xlsx \
example_traces/example_event_traces.txt \
--steps=${STEPS} \
--output_format=7

echo -e "\n\nTesting simulation, fixed update scheme using event traces\n"
# event trace input for fixed updates
python3 ${FRAMEWORKPATH}/Simulation/Simulator_Python/simulator_interface.py \
example_models/example_model.xlsx \
example_traces/example_fixed_updates.txt \
--steps=${STEPS} \
--sim_scheme=fixed_updates \
--event_traces_file=example_traces/example_event_traces.txt 

echo -e "\n\nTesting simulation, transpose trace output\n"
# transpose trace file output (used by model checking and sensitivity)
python3 ${FRAMEWORKPATH}/Simulation/Simulator_Python/simulator_interface.py \
example_models/example_model.xlsx \
example_traces/example_traces_transpose.txt \
--output_format=2 \
--steps=${STEPS} \
--runs=1

echo -e "\n\nTesting simulation, model with delays\n"
# model with delays
MODELNAME='delay'
STEPS=10
python3 ${FRAMEWORKPATH}/Simulation/Simulator_Python/simulator_interface.py \
example_models/example_model_${MODELNAME}.xlsx \
example_traces/example_traces_${MODELNAME}.txt \
--output_format=1 \
--runs=2 \
--sim_scheme='rand_sync' \
--steps=${STEPS}

echo -e "\n\nTesting simulation, model with update rates\n"
# model with delays
MODELNAME='update_rates'
STEPS=10
python3 ${FRAMEWORKPATH}/Simulation/Simulator_Python/simulator_interface.py \
example_models/example_model_${MODELNAME}.xlsx \
example_traces/example_traces_${MODELNAME}.txt \
--output_format=3 \
--runs=100 \
--sim_scheme='ra' \
--steps=${STEPS}

