SCENARIOS='CAG_4_level CAG_4_trend CAG_4_hybrid CAG_4_level_AND CAG_4_trend_AND CAG_4_hybrid_AND CAG_4_level_OR_mix CAG_4_trend_OR_mix CAG_4_hybrid_OR_mix CAG_4_level_NOT CAG_4_trend_NOT CAG_4_hybrid_NOT CAG_4_level_AND_target CAG_4_trend_AND_target CAG_4_hybrid_AND_target'
FRAMEWORKPATH='./DiSH-trend'
STEPS=30
# MODELNAME='shock_demo'
WORKPATH='toy_models/'
CURRENTOUT='toy_model_results/'

mkdir "${WORKPATH}"
mkdir "${WORKPATH}${CURRENTOUT}"
for S in $SCENARIOS
do
	# SCENARIONAME=${S}
	mkdir "${WORKPATH}${CURRENTOUT}${S}"
	INPUTFILE="${WORKPATH}${S}.xlsx"	
	OUTPUTFILE="${WORKPATH}${CURRENTOUT}${S}/${S}_traces.txt"
	OUTPUTMODEL="${WORKPATH}${CURRENTOUT}${S}/${S}.xlsx"

	cp ${INPUTFILE} ${OUTPUTMODEL}

	python3 ${FRAMEWORKPATH}/Simulator_Python/simulator_interface.py \
	${INPUTFILE} \
	${OUTPUTFILE} \
	--output_format=3 \
	--runs=200 \
	--steps=${STEPS} \
	--sim_scheme=ra \
	--timed
done