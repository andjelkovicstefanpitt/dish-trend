import re
from collections import defaultdict
import numpy as np
import matplotlib.pyplot as plt


def get_traces(input_file: str, default_levels=3):
    """Load simulation values from a trace file

    Trace file should contain (optionally) individual runs, followed by the
    frequency summaries, followed by the squares summaries for each element
    (output_format 1 or 3 from the simulator)
    """

    # dictionary to store traces for each element
    trace_data = defaultdict(lambda: defaultdict())

    # read traces from trace file
    with open(input_file) as in_file:
        trace_file_lines = in_file.readlines()
    trace_file_lines = [x.strip() for x in trace_file_lines]

    run = 0
    frequency_summary = False
    squares_summary = False
    for trace_line_content in trace_file_lines:

        # check each line for the Run #, Frequency/Squares summaries, or values
        if re.match(r'Run #[0-9]+', trace_line_content):
            # save the last run # as the total number of runs
            run = re.findall(r'Run #([0-9]+)', trace_line_content)[0]

        elif re.match('Frequency Summary:', trace_line_content):
            # set frequency summary flag
            frequency_summary = True

        elif re.match('Squares Summary:', trace_line_content):
            # set squares summary flag
            squares_summary = True

        else:
            # get trace values and element name
            trace_values_str = trace_line_content.split(' ')
            element_name = trace_values_str[0].split('|')[0]

            if element_name != '':
                # get num states for this element if available
                if len(trace_values_str[0].split('|'))>1:
                    levels = int(trace_values_str[0].split('|')[1])
                else:
                    levels = default_levels

                # get simulation values
                trace_values = [float(val) for val in trace_values_str[1:]]

                # store trace data
                trace_data[element_name]['levels'] = levels
                # will save the number read from the last occurrence of "Run#" in the file
                # adding 1 to correct for zero indexed run number
                trace_data[element_name]['runs'] = int(run) + 1
                if squares_summary:
                    trace_data[element_name]['squares'] = trace_values
                    # calculate avg and stdev values
                    runs = trace_data[element_name]['runs']
                    levels = trace_data[element_name]['levels']
                    freq_vals = trace_data[element_name]['frequency']
                    avg_vals = [float(val)/runs for val in freq_vals]
                    stdev_vals = [np.sqrt(float(val)/runs - avg_vals[idx]**2) for idx, val in enumerate(trace_values)]

                    trace_data[element_name]['avg'] = avg_vals
                    trace_data[element_name]['stdev'] = stdev_vals
                    trace_data[element_name]['avg_percent'] = [100*val/(levels-1) for val in avg_vals]
                    trace_data[element_name]['stdev_percent'] = [100*val/(levels-1) for val in stdev_vals]

                elif frequency_summary:
                    trace_data[element_name]['frequency'] = trace_values
                else:
                    if 'traces' in trace_data[element_name]:
                        trace_data[element_name]['traces'].update({run : trace_values})
                    else:
                        trace_data[element_name]['traces'] = {run : trace_values}

    return trace_data