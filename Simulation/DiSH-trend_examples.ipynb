{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import os\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import logging\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "logger = logging.getLogger()\n",
    "logger.setLevel(logging.INFO)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# DiSH-trend"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simulate"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from Simulation.Simulator_Python import simulator_interface as sim\n",
    "from traces import get_traces"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# simulation parameters\n",
    "model_file = '../example_models/example_model.xlsx'\n",
    "output_path = '../example_traces'\n",
    "steps = 30\n",
    "runs = 200\n",
    "scenarios = [0]\n",
    "output_format = 3 # just frequency summaries, not singular runs\n",
    "scheme = 'ra'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_model = pd.read_excel(model_file).fillna('')\n",
    "df_model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Model spreadsheet explained"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are 6 **mandatory** columns:\n",
    "\n",
    "1) **Element Name** (holds the name for displaying the simulation results)\n",
    "\n",
    "2) **Variable** (holds the name by which this variable will be referred to in the simulator)\n",
    "\n",
    "3) **Positive** (holds the string that encodes all POSITIVE regulation of the element)\n",
    "\n",
    "4) **Negative** (holds the string that encodes all NEGATIVE regulation of the element)\n",
    "\n",
    "5) **Levels** (the number of the element's discrete levels)\n",
    "\n",
    "6) **Scenario 0** (contains the initial values for the simulation, and the enforced values at given timesteps)\n",
    "\n",
    "Note: there can be multiple scenarios in the spreadsheet, each identified by its own initialization (Scenario 0, Scenario 1, Scenario 2, ...)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Positive** and **Negative** columns are filled the same way. They encode the update functions, where the sources of influence are joined by addition when independently affecting the element (*A*+*B*), or multiplication when jointly affecting the element (*A*\\**B*). Each interaction (influence) consists of level-based and/or trend-based component. These are discerned by the level weights and trend weights. *A* singular influence *A*->*C* is encoded as w<sub>t</sub>&w<sub>l</sub>\\*A, where w<sub>t</sub> is trend weight, w<sub>l</sub> is level weight, and A is the regulator name. This is decoded by the simulator into an expression:\n",
    "\n",
    "$w_{t}*dA+w_{l}*A$\n",
    "\n",
    "where *dA* is the trend of the element **A** (the difference between current value of **A** and its last updated value), and *A* is the level of the element **A**. The weights *w<sub>t</sub>* and *w<sub>l</sub>* should belong to intervals \\[-1,1\\] and \\[0,1\\], respectively.\n",
    "\n",
    "**Note:** the absence of trend-based regulation can be encoded in two ways:\n",
    "\n",
    "1) by setting $w_{t}=0$\n",
    "\n",
    "2) by setting only level-based regulation weight (e.g. writing w<sub>l</sub>\\*A)\n",
    "\n",
    "The absence of the level-based regulation can only be encoded by setting $w_{l}=0$ (e.g. writing w<sub>l</sub>&0\\*A).\n",
    "\n",
    "If an element's name is written without weights, it assumes default values of $w_{t}=0$ and $w_{l}=1$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Due to limitations in classifying regulations as positive or negative, in the current version of the simulator the model spreadsheet requires virtual counterterms on the opposite side of the regulation. The original terms have non-negative weights only, while the counterterms have $w_{t}\\le 0$, and $w_{l}=0$\n",
    "\n",
    "Example \\#1: *A **positively** regulates B, with trend weight 0.8, and level weight 0.4*.\n",
    "\n",
    "In **Positive** column of B: 0.8&0.4\\*A\n",
    "\n",
    "In **Negative** column of B: -0.8&0\\*A\n",
    "\n",
    "Example \\#2: *A **negatively** regulates B, with trend weight 0.3, and level weight 0.5*.\n",
    "\n",
    "In **Positive** column of B: -0.3&0\\*A\n",
    "\n",
    "In **Negative** column of B: 0.3&0.5\\*A\n",
    "\n",
    "Example \\#3: *A **positively** regulates C with trend weight 0.8, and level weight 0.4, while B independently of A **negatively** regulates C with trend weight 0.3, and level weight 0.5*.\n",
    "\n",
    "In **Positive** column of C: 0.8&0.4\\*A+-0.3&0\\*B\n",
    "\n",
    "In **Negative** column of C: 0.3&0.5\\*B+-0.8&0\\*A\n",
    "\n",
    "Note: the weights don't necessarily add up to 1. They are dynamic parameters that are describing individual interactions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "scenarios_sorted = [str(x) for x in sorted(scenarios)]\n",
    "\n",
    "# set up output paths\n",
    "if not os.path.exists(output_path):\n",
    "    os.mkdir(output_path)\n",
    "output_basename = os.path.join(output_path,'example_traces')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# simulate \n",
    "sim.setup_and_run_simulation(\n",
    "         model_file, \n",
    "         output_basename + '.txt', \n",
    "         steps, \n",
    "         runs, \n",
    "         scheme, \n",
    "         output_format,\n",
    "         ','.join(scenarios_sorted)\n",
    "         )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualize simulation results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define parameters and load from files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# model and output file locations (same as simulation example above)\n",
    "model_file = '../example_models/example_model.xlsx'\n",
    "output_path = '../example_traces'\n",
    "output_basename = os.path.join(output_path,'example_traces')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_model = pd.read_excel(model_file).fillna('')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# visualization parameters\n",
    "scenarios = [0]\n",
    "colors = None # can be a list of colors, length matching the number of scenarios\n",
    "linestyles = None # can be a list, length matching the number of scenarios\n",
    "linewidth = 2\n",
    "normalize_levels = True # whether to plot y axis as a percentage"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# get scenario labels from model\n",
    "input_col_initial = [x.strip() for x in df_model.columns if ('scenario' in x.lower())]\n",
    "scenario_labels = [input_col_initial[x] for x in scenarios]\n",
    "\n",
    "# construct list of trace files automatically generated by the simulator\n",
    "scenarios_sorted = [str(x) for x in sorted(scenarios)]\n",
    "if len(scenarios_sorted) > 1:\n",
    "    trace_files = [\n",
    "                output_basename + '_' + str(this_scenario) + '.txt'\n",
    "                for this_scenario in scenarios_sorted\n",
    "                ]\n",
    "else:\n",
    "    trace_files = [output_basename + '.txt']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get trace data\n",
    "traces_list = [get_traces(trace_file) for trace_file in trace_files]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "traces_list[0].keys()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "traces_list[0]['CAUSE'].keys()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Average plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "element_name = 'CAUSE'\n",
    "plt.plot(np.array(traces_list[0][element_name]['avg_percent'])/100.)\n",
    "plt.xlabel('Steps')\n",
    "plt.ylabel('Level')\n",
    "plt.ylim(-0.05,1.05)\n",
    "plt.title(element_name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Average plot with uncertainty"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "element_name = 'OUTCOME'\n",
    "avg_perc = np.array(traces_list[0][element_name]['avg_percent'])/100.\n",
    "stdev_perc = np.array(traces_list[0][element_name]['stdev_percent'])/100.\n",
    "plt.plot(avg_perc)\n",
    "plt.fill_between(range(len(avg_perc)), avg_perc-stdev_perc, avg_perc+stdev_perc, alpha=0.2)\n",
    "plt.xlabel('Steps')\n",
    "plt.ylabel('Level')\n",
    "plt.ylim(-0.0001,1.0001)\n",
    "plt.title(element_name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Calibrated plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "element_name = 'OUTCOME'\n",
    "avg_perc = np.array(traces_list[0][element_name]['avg_percent'])/100.\n",
    "stdev_perc = np.array(traces_list[0][element_name]['stdev_percent'])/100.\n",
    "\n",
    "# Calibrate x-axis\n",
    "x = pd.date_range('2021-01-01','2021-04-01', periods=len(avg_perc))\n",
    "\n",
    "# Calibrate y-axis\n",
    "min_value = 130\n",
    "max_value = 170\n",
    "#levels = traces_list[0][element_name]['levels']\n",
    "y = avg_perc * (max_value - min_value) + min_value\n",
    "y_stdev = stdev_perc * (max_value - min_value)\n",
    "\n",
    "plt.plot(x, y)\n",
    "plt.fill_between(x, y-y_stdev, y+y_stdev, alpha=0.2)\n",
    "plt.xlabel('Steps')\n",
    "plt.ylabel('Level')\n",
    "plt.xticks(rotation=30)\n",
    "plt.ylim(min_value, max_value)\n",
    "plt.title(element_name)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "py36",
   "language": "python",
   "name": "py36"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "196px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
